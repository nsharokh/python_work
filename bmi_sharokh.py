# создать меню
#  sozdat' CRUD
# obernut s cicle

def bmi_app ():
    menu = """
    ________________________________________________
    |                                               |
    |  1. Вывести список                            |
    |  2. Удалить                                   |
    |  3. Добавить\редактировать                    |
    |  4. Отобразить                                |
    |  5. Выйти                                     |
    |                                               |
    _________________________________________________
    """
    users = {
        "user1": {'height': 1.80, 'weight': 100, 'bmi': 30},
        "user2": {'height': 1.60, 'weight': 80, 'bmi': 40},
        }
    def userprnt():
        for user in users:
            print(user)

    def bmi_calc ():
            height = input("Введите рост: ")
            weight = input("Введите вес: ")
            bmi = int(weight)/float(height)**2
            users[user] = {
                'height': float(height),
                'weight': int(weight),
                'bmi': round(bmi, 1)
            }

    while True:
        print(menu)
        res = int(input("Введите пункт меню: "))

        if res == 1: # vivesti spisok
            if not users:
                print("Список пуст")
            else:
                print("Список пользователей:\n")
                userprnt()

        elif res == 2: # delete usera
            user = input("Введите имя пользователя для удаления: ")
            del users[user]
            print(f"Пользователь {user} удален!")
            print("Пользователи в системе после удаления")
            userprnt()

        elif res == 3: # add/edit usera
            print("Добавить или редактировать пользователя")
            user = input("Введите имя пользователя: ")
            user_exists = users.get(user)
            if user_exists:
                print("Редактируем пользователя")
                print("Текущий рост: {}, текущий вес: {}".format(users[user]['height'], users[user]['weight']))
                bmi_calc()
            else:
                bmi_calc()
        elif res == 4: # view usera
            user = input("Введите имя пользователя: ")
            print("Текущий рост: {}, текущий вес: {}, текущий BMI: {}".format(
                users[user]['height'],
                users[user]['weight'],
                users[user]['bmi']))

        elif res == 5: # delete usera
            break

bmi_app()